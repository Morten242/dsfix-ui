﻿// TODO?: Ability to delete variables? (Easy to remove from list, but need to also remove it from file.) Super-low priority.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace RegistryIniTool
{
    public class IniEditor
    {
        private readonly List<OptionElement> _optionList;
        private readonly string _path;

        public IniEditor(string path)
        {
            _optionList = new List<OptionElement>();
            _path = path;
            if (File.Exists(_path))
            {
                LoadIniFile();
            }
        }

        private void LoadIniFile()
        {
            using (var reader = new StreamReader(_path))
            {
                OptionElement? element;
                while ((element = ReadNextVariable(reader)).HasValue)
                {
                    _optionList.Add(element.Value);
                }
            }
        }

        public void Save()
        {
            var optionCopy = new List<OptionElement>();
            optionCopy.AddRange(_optionList.ToArray());

            var lines = File.ReadAllLines(_path);
            using (var writer = new StreamWriter(_path))
            {
                foreach (var line in lines)
                {
                    Match match;
                    if ((match = MatchVariable(line)).Success)
                    {
                        var elementFromMatch = GetElementFromMatch(match);
                        var element = GetElement(elementFromMatch.OptionName, optionCopy);
                        if (element.HasValue)
                        {
                            PrintElement(writer, element.Value);
                            optionCopy.Remove(element.Value);
                        }
                        else
                        {
                            writer.WriteLine(line);
                        }
                    }
                    else
                    {
                        writer.WriteLine(line);
                    }
                }

                // output the remaining elements
                foreach (var element in optionCopy)
                {
                    PrintElement(writer, element);
                }
            }
        }

        private static void PrintElement(TextWriter writer, OptionElement element)
        {
            writer.WriteLine("{0}{1} {2}", (element.IsComment ? "#" : ""), element.OptionName, element.Value);
        }

        public T GetValue<T>(string option)
        {
            var value = _optionList.Find(x => x.OptionName == option).Value;
            if (String.IsNullOrEmpty(value))
            {
                throw new Exception(string.Format("No value was found matching optionname {0}", option));
            }
            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }

        public OptionElement? GetElement(string option, List<OptionElement> optionList = null)
        {
            if (optionList == null)
            {
                optionList = _optionList;
            }

            var element = optionList.Find(x => x.OptionName == option);
            return element.Value == null ? (OptionElement?)null : element;
        }

        public bool SetValue<T>(string option, T newValue, bool isComment = false, bool addIfNotFound = true)
            where T : IConvertible
        {
            var index = _optionList.FindIndex(x => x.OptionName == option);
            if (index < 0)
            {
                // In case automatic creation is wanted when trying to set a value
                if (addIfNotFound)
                {
                    _optionList.Add(new OptionElement(option, newValue.ToString(CultureInfo.InvariantCulture), isComment));
                }
                return false;
            }

            // Get the element and then edit it:
            var newElement = _optionList[index];
            newElement.Value = newValue.ToString(CultureInfo.InvariantCulture);
            newElement.IsComment = isComment;
            _optionList[index] = newElement;
            return true;
        }

        public static void Clear(string path)
        {
            if (!File.Exists(path))
            {
                return;
            }
            TextWriter writer = new StreamWriter(path);
            writer.Write("");
            writer.Close();
        }

        public static void AddComment(string comment)
        {
            throw new NotImplementedException("oops");
        }

        public bool Exchange(string previousOptionName, string newOptionName)
        {
            var index = _optionList.FindIndex(x => x.OptionName == previousOptionName);
            if (index < 0)
            {
                return false;
            }
            var newElement = _optionList[index];
            newElement.OptionName = newOptionName;
            _optionList[index] = newElement;
            return true;
        }

        private static OptionElement? ReadNextVariable(TextReader reader)
        {
            int linesRead;
            return ReadNextVariable(reader, out linesRead);
        }

        private static OptionElement? ReadNextVariable(TextReader reader, out int linesRead)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            linesRead = 0;
            Match match;
            string line;

            do
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    return null;
                }
                linesRead++;
            } while (!(match = MatchVariable(line)).Success);
            var element = GetElementFromMatch(match);
            return element;
        }

        private static OptionElement GetElementFromMatch(Match match)
        {
            var element = new OptionElement
            {
                OptionName = match.Groups["option"].Value,
                Value = match.Groups["value"].Value,
                IsComment = match.Groups["comment"].Success
            };

            if (match.Groups["hasF"].Success) // in the odd case that floats without a decimal value is written as 0f/1f
            {
                element.Value = element.Value.TrimEnd('f');

                //element.Value = element.Value.Remove(element.Value.Length - 1);
            }

            return element;
        }

        private static Match MatchVariable(string line)
        {
            // The regex is a little gross, but it matches variables (lines with text from the beginning.) It also
            // matches commented out variables (comment-tags (#) that are immediately followed by text.) Most likely not
            // a generic solution.
            const string variableRegex =
                @"^(?<comment>#)?(?<option>\w+)\s((?<value>\d+((?<hasF>f)?|\.\d*))(?:f)?|(?<value>.+))$";

            return Regex.Match(line, variableRegex, RegexOptions.Singleline);
        }
    }

    public struct OptionElement
    {
        public bool IsComment;
        public string OptionName;
        public string Value;

        public OptionElement(string option, string value, bool isComment = false)
        {
            OptionName = option;
            Value = value;
            IsComment = isComment;
        }
    }
}