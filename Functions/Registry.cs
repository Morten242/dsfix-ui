﻿using Microsoft.Win32;

namespace RegistryIniTool
{
    public static class RegistryTool
    {
        public static bool TryGetValueLocalMachine(string registryPath, string keyName, out string keyValue)
        {
            //open key to read value
            var key = Registry.LocalMachine.OpenSubKey(registryPath);

            if (key != null)
            {
                keyValue = key.GetValue(keyName).ToString();
                return true;
            }

            keyValue = "";
            return false;
        }

        public static bool TryGetValueCurrentUser(string registryPath, string keyName, out string keyValue)
        {
            //open key to read value
            var key = Registry.CurrentUser.OpenSubKey(registryPath);

            if (key != null)
            {
                keyValue = key.GetValue(keyName).ToString();
                return true;
            }

            keyValue = "";
            return false;
        }
    }
}