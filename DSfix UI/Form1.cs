﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DSFix_Tool.Properties;
using Morten242_Csharp_Library;
using RegistryIniTool;

// TODO?: error reporting system

namespace DSFix_Tool
{
    public partial class Form1 : Form
    {
        private const string DsIniPath = @"\data\DSfix.ini";
        private const string DsKeysIniPath = @"\data\DSfixKeys.ini";

        // I don't know if these two still work after the move to steamworks:
        private const string RegistryKey = @"SOFTWARE\Wow6432Node\NAMCO BANDAI GAMES\DARK SOULS";

        private const string RegistryKeyName = "Install_Path";
        private IniEditor _appSettingsEditor;
        private IniEditor _dsfixEditor;

        // Used to check whether or not to actually trigger some "on change"-events (since loading stuff usually changes things)
        private bool _isLoading;

        private IniEditor _keysEditor;
        private string _keysPath;
        private string _path;
        private decimal _screenHeight;
        private decimal _screenWidth;
        private int _timeUnitSelectedIndex;
        private readonly string _appIni = Path.Combine(Application.StartupPath, "settings.ini");

        [Localizable(false)]
        private readonly string[] _languages =
        {
            "none", "en-GB", "fr", "it", "de", "es", "ko", "zh-tw", "pl", "ru"
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _appSettingsEditor = new IniEditor(_appIni);
            GetDsFixPath();
            _dsfixEditor = new IniEditor(_path);
            _keysEditor = new IniEditor(_keysPath);
            InitializeDropdownBoxes();

            try
            {
                LoadAll(_dsfixEditor);
                LoadKeybindSettings(_keysEditor);
            }
            catch
            {
                MessageBox.Show(
                    Resources.invalidValue);
            }

            CheckAntiAliasingSetting();
            CheckMouseFix();
        }

        private void InitializeDropdownBoxes()
        {
            aaQuality.SelectedIndex = 0;
            ssaoStrength.SelectedIndex = 0;
            dofOverrideRes.SelectedIndex = 0;
            _isLoading = true;
            timeUnit.SelectedIndex = 0;
            _isLoading = false;
            overrideLanguage.SelectedIndex = 0;
            SetupResolution();
        }

        private void GetDsFixPath()
        {
            if (DsFixRelativePath() || DsFixStoredPath() || DsFixRegistryKey())
            {
                return;
            }

            // Dsfix wasn't found at all
            MessageBox.Show(Resources.CouldNotLocateDirectory);
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var dsPath = openFileDialog1.FileName.Remove(openFileDialog1.FileName.Length - 10);
                _path = openFileDialog1.FileName;
                _keysPath = Path.Combine(dsPath, "DSfixKeys.ini");
                _appSettingsEditor.SetValue(@"path", dsPath);
                Activate();
                BringToFront();
            }
            else
            {
                MessageBox.Show(Resources.ErrorLocating, Resources.error);
                Close();
            }
        }

        private bool DsFixRegistryKey()
        {
            // Look up Dark Souls' path in the registry and look there.
            string temp;
            if (!RegistryTool.TryGetValueLocalMachine(RegistryKey, RegistryKeyName, out temp))
            {
                return false;
            }
            _path = Path.Combine(temp, DsIniPath);
            _keysPath = Path.Combine(temp, DsKeysIniPath);
            return true;
        }

        private bool DsFixStoredPath()
        {
            // Finding the directory of the INI file stored in settings
            var path2 = _appSettingsEditor.GetValue<String>(@"path");
            if (path2 == null || !File.Exists(Path.Combine(path2, "DSfix.ini")))
            {
                return false;
            }

            _appSettingsEditor.SetValue(@"path", path2.TrimEnd('\\'));
            _path = Path.Combine(path2, "Dsfix.ini");
            _keysPath = Path.Combine(path2, "dsfixkeys.ini");
            return true;
        }

        private bool DsFixRelativePath()
        {
            // relative path
            if (!File.Exists(@"Dsfix.ini"))
            {
                return false;
            }

            // If the user places this tool in the Dark Souls directory
            _path = @"Dsfix.ini";
            _keysPath = @"DSfixKeys.ini";
            return true;
        }

        private void SetupResolution()
        {
            // Screen resolution
            _screenWidth = SystemInformation.PrimaryMonitorSize.Width;
            _screenHeight = SystemInformation.PrimaryMonitorSize.Height;

            var resolutions = Resolution.GetResolutions();

            if (resolutions != null)
            {
                resolutionDrop.Items.AddRange(
                    resolutions.Select(
                        x => string.Format(CultureInfo.InvariantCulture, @"{0}x{1}", x.Item1, x.Item2) as object)
                        .ToArray());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            resolutionDrop.SelectedIndex = resolutionDrop.FindStringExact(_screenWidth + @"x" + _screenHeight);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadAll(_dsfixEditor);
            LoadKeybindSettings(_keysEditor);
        }

        #region Loading

        private void LoadAll(IniEditor editor)
        {
            _isLoading = true;

            LoadGraphicsSettings(editor);

            LoadAntiAliasingAndAmbientOcclusionSettings(editor);

            LoadDepthOfFieldSettings(editor);

            LoadHudSettings(editor);

            LoadWindowsAndMouseSettings(editor);

            LoadBackupsSettings(editor);

            LoadTextureSettings(editor);

            LoadMiscSettings(editor);

            _isLoading = false;
        }

        private void LoadKeybindSettings(IniEditor editor)
        {
            foreach (Control c in keyPanel.Controls)
            {
                if (c is TextBox)
                {
                    if (c.Name.StartsWith(@"key", StringComparison.CurrentCulture))
                    {
                        var varName = c.Name.Remove(0, 3);
                        var result = editor.GetElement(varName);

                        if (result.HasValue)
                        {
                            c.Text = result.Value.Value.Replace(@"VK_", "");

                            ((CheckBox)(keyPanel.Controls.Find(@"enable" + varName, false)[0])).Checked =
                                !result.Value.IsComment;
                        }
                    }
                }
            }
        }

        private void LoadMiscSettings(IniEditor editor)
        {
            skipIntro.Checked = editor.GetValue<Int32>(@"skipIntro") == 1;

            screenshotDir.Text = editor.GetValue<String>(@"screenshotDir");

            // wrapper
            dinput8dllWrapper.Text = editor.GetValue<String>(@"dinput8dllWrapper");

            // logging
            logLevel.Value = editor.GetValue<Int32>(@"logLevel");

            // Language
            var lang = editor.GetValue<String>(@"overrideLanguage");

            var i = 0;
            while (_languages[i] != lang)
            {
                i++;
            }

            overrideLanguage.SelectedIndex = i;
        }

        private void LoadTextureSettings(IniEditor editor)
        {
            // Textures
            enableTextureDumping.Checked = editor.GetValue<Int32>(@"enableTextureDumping") == 1;
            enableTextureOverride.Checked = editor.GetValue<Int32>(@"enableTextureOverride") == 1;
        }

        private void LoadBackupsSettings(IniEditor editor)
        {
            // Backups
            enableBackups.Checked = editor.GetValue<Int32>(@"enableBackups") == 1;

            backupInterval.Value = MoreMath.Clamp(editor.GetValue<Decimal>(@"backupInterval"), backupInterval.Minimum,
                backupInterval.Maximum);
            if (timeUnit.SelectedIndex == 1)
            {
                if (backupInterval.Value != backupInterval.Minimum)
                {
                    backupInterval.Value /= 60; // Convert to minutes if minutes is selected
                }
            }
            maxBackups.Value = MoreMath.Clamp(editor.GetValue<Decimal>(@"maxBackups"), maxBackups.Minimum,
                maxBackups.Maximum);
        }

        private void LoadWindowsAndMouseSettings(IniEditor editor)
        {
            // Window and mouse
            borderlessFullscreen.Checked = editor.GetValue<Int32>(@"borderlessFullscreen") == 1;
            disableCursor.Checked = editor.GetValue<Int32>(@"disableCursor") == 1;
            captureCursor.Checked = editor.GetValue<Int32>(@"captureCursor") == 1;
        }

        private void LoadAntiAliasingAndAmbientOcclusionSettings(IniEditor editor)
        {
            // SMAA / SSAO
            aaQuality.SelectedIndex = editor.GetValue<Int32>(@"aaQuality");
            ssaoStrength.SelectedIndex = editor.GetValue<Int32>(@"ssaoStrength");

            ssaoScale.SelectedIndex = editor.GetValue<Int32>(@"ssaoScale") - 1;

            //
            var i = 0;
            var aoType = editor.GetValue<String>(@"ssaoType");
            foreach (var item in ssaoType.Items)
            {
                if (String.Equals(item.ToString(), aoType, StringComparison.CurrentCultureIgnoreCase))
                {
                    ssaoType.SelectedIndex = i;
                    break;
                }
                i++;
            }

            // aaType, aaQuality and FPS toggle
            aaQuality.SelectedIndex = editor.GetValue<Int32>(@"aaQuality");
            var aaTypeValue = editor.GetValue<String>(@"aaType");
            if (aaTypeValue != null)
            {
                aaTypeValue = aaTypeValue.ToLower(CultureInfo.CurrentCulture);
                if (aaTypeValue == @"smaa")
                {
                    aaType.SelectedIndex = 0;
                }
                else if (aaTypeValue == @"fxaa")
                {
                    aaType.SelectedIndex = 1;
                }
            }
        }

        private void LoadGraphicsSettings(IniEditor editor)
        {
            var resolutionValue = string.Format(CultureInfo.CurrentCulture, @"{0}x{1}",
                editor.GetValue<Decimal>(@"renderWidth"),
                editor.GetValue<Decimal>(@"renderHeight"));

            var index = resolutionDrop.FindStringExact(resolutionValue);
            if (index == -1)
            {
                // in case they are using an unsupported resolution then I give them the benefit of the doubt and add it.
                // (for downscaling)
                resolutionDrop.Items.Add(resolutionValue);
                index = resolutionDrop.Items.Count - 1;
            }
            resolutionDrop.SelectedIndex = index;

            var fpsLimitValue = editor.GetValue<Int32>(@"FPSlimit");
            var maximumFrames = fpsLimitValue;
            maxFPS.Value = MoreMath.Clamp(maximumFrames, 30, 120);

            var unlockFpsValue = editor.GetValue<Int32>(@"unlockFPS");
            unlockFPS.Checked = (unlockFpsValue == 1);

            // The FPS threshold stuff. Disables Anti-aliasing when framerate drops
            // ReSharper disable once StringLiteralTypo
            FPSthreshold.Value = MoreMath.Clamp(editor.GetValue<Decimal>(@"FPSthreshold"), FPSthreshold.Minimum,
                FPSthreshold.Maximum);

            // downscaling resolution
            presentWidth.Value = editor.GetValue<Decimal>(@"presentWidth");
            presentHeight.Value = editor.GetValue<Decimal>(@"presentHeight");
        }

        private void LoadDepthOfFieldSettings(IniEditor editor)
        {
            var i = 0;

            var overrideResolution = editor.GetValue<Int32>(@"dofOverrideResolution");
            var overrideResString = overrideResolution.ToString(CultureInfo.CurrentCulture);

            // Match up loaded value and the dropdown box
            foreach (var item in dofOverrideRes.Items)
            {
                if (item.ToString() == overrideResString || (item.ToString() == @"360" && overrideResString == @"0"))
                {
                    dofOverrideRes.SelectedIndex = i;
                    customDofOverrideRes.Checked = false;
                    break;
                }
                customDofOverrideRes.Checked = true;
                i++;
            }

            var loadedDofOverrideRes = Convert.ToInt32(overrideResolution);
            dofOverrideResNum.Value = (loadedDofOverrideRes >= 360) ? loadedDofOverrideRes : 360;

            enableDofScaling.Checked = editor.GetValue<Int32>(@"disableDofScaling") != 1;

            var amount = editor.GetValue<Int32>(@"dofBlurAmount");
            amount = MoreMath.Clamp(amount, 0, 10);
            dofBlurAmount.Value = amount;
        }

        private void LoadHudSettings(IniEditor editor)
        {
            // HUD
            enableHudMod.Checked = editor.GetValue<Int32>(@"enableHudMod") == 1;

            minimalHud.Checked = editor.GetValue<Int32>(@"enableMinimalHud") == 1;

            // Hud scale factor
            var hudScaleFactorValue = editor.GetValue<Decimal>(@"hudScaleFactor");
            hudScaleFactor.Value = Convert.ToInt32(hudScaleFactorValue * 100, CultureInfo.InvariantCulture);

            // Hud top left
            var hudTopLeftOpacityValue = editor.GetValue<Decimal>(@"hudTopLeftOpacity");
            topLeftCorner.Value = Convert.ToInt32(hudTopLeftOpacityValue * 100, CultureInfo.InvariantCulture);

            // Hud bottom left
            var hudBottomLeftOpacityValue = editor.GetValue<Decimal>(@"hudBottomLeftOpacity");
            bottomLeftCorner.Value = Convert.ToInt32(hudBottomLeftOpacityValue * 100, CultureInfo.InvariantCulture);

            // Hud bottom right
            var hudBottomRightOpacityValue = editor.GetValue<Decimal>(@"hudBottomRightOpacity");
            bottomRightCorner.Value = Convert.ToInt32(hudBottomRightOpacityValue * 100, CultureInfo.InvariantCulture);
        }

        #endregion Loading

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (customDofOverrideRes.Checked)
            {
                dofOverrideResNum.Visible = true;
                dofOverrideRes.Visible = false;
            }
            else
            {
                dofOverrideResNum.Visible = false;
                dofOverrideRes.Visible = true;
            }
        }

        private void enableHudMod_CheckedChanged(object sender, EventArgs e)
        {
            if (enableHudMod.Checked)
            {
                minimalHud.Enabled = true;
                hudScaleFactor.Enabled = true;
                topLeftCorner.Enabled = true;
                bottomLeftCorner.Enabled = true;
                bottomRightCorner.Enabled = true;
            }
            else
            {
                minimalHud.Enabled = false;
                hudScaleFactor.Enabled = false;
                topLeftCorner.Enabled = false;
                bottomLeftCorner.Enabled = false;
                bottomRightCorner.Enabled = false;
            }
        }

        private void trackBars_ValueChanged(object sender, EventArgs e)
        {
            var hSf = hudScaleFactor.Value / 100.0f;
            var tLc = topLeftCorner.Value / 100.0f;
            var bLc = bottomLeftCorner.Value / 100.0f;
            var bRc = bottomRightCorner.Value / 100.0f;
            label13.Text = Convert.ToString(hSf, CultureInfo.InvariantCulture);
            label14.Text = Convert.ToString(tLc, CultureInfo.InvariantCulture);
            label15.Text = Convert.ToString(bLc, CultureInfo.InvariantCulture);
            label16.Text = Convert.ToString(bRc, CultureInfo.InvariantCulture);
        }

        private void timeUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (timeUnit.SelectedIndex != _timeUnitSelectedIndex)
            {
                if (timeUnit.SelectedIndex == 0)
                {
                    if (!_isLoading)
                    {
                        backupInterval.Value = backupInterval.Value * 60;
                    }
                    backupInterval.Minimum = 600;
                }
                else
                {
                    backupInterval.Minimum = 10;
                    if (!_isLoading)
                    {
                        backupInterval.Value = backupInterval.Value / 60;
                    }
                }
            }
        }

        private void dsFixSaveButton_Click(object sender, EventArgs e)
        {
            SaveAll(_dsfixEditor);

            // Keybindings
            SaveKeys(_keysEditor);
        }

        #region Saving

        private void SaveAll(IniEditor editor)
        {
            // Graphics
            SaveGraphicsSettings(editor);

            // anti-aliasing and ambient occlusion
            SaveAaAoSettings(editor);

            // HUD
            SaveHudSettings(editor);

            // Window and mouse
            SaveWindowAndMouseSettings(editor);

            // Backups
            SaveBackupSettings(editor);

            // Texture override etc.
            SaveTextureSettings(editor);

            // MISC
            SaveMiscSettings(editor);

            editor.Save();
        }

        private void SaveKeys(IniEditor editor)
        {
            foreach (Control c in keyPanel.Controls)
            {
                // There's no other textboxes on this particular panel, and their names all start with "key", so we loop through those
                if (c is TextBox)
                {
                    if (c.Name.StartsWith(@"key", StringComparison.CurrentCulture))
                    {
                        var enabled =
                            ((CheckBox)(keyPanel.Controls.Find(@"enable" + c.Name.Remove(0, 3), false)[0])).Checked;
                        editor.SetValue(c.Name.Remove(0, 3), @"VK_" + c.Text, !enabled);
                    }
                }
            }
            editor.Save();
        }

        private void SaveMiscSettings(IniEditor editor)
        {
            editor.SetValue(@"skipIntro", skipIntro.Checked ? @"1" : @"0");

            var screenshot = screenshotDir.Text;
            if (screenshot != @"." && !Directory.Exists(screenshot))
            {
                try
                {
                    Directory.CreateDirectory(screenshot);
                }
                catch (Exception)
                {
                    screenshot = @".";
                    MessageBox.Show(
                        String.Format(
                            Resources.screenshotFolderError,
                            screenshot), Resources.error);
                }
            }
            editor.SetValue(@"screenshotDir", screenshot);

            editor.SetValue(@"overrideLanguage", _languages[overrideLanguage.SelectedIndex]);

            editor.SetValue(@"dinput8dllWrapper", !(String.IsNullOrEmpty(dinput8dllWrapper.Text)) ? dinput8dllWrapper.Text : @"none");
            editor.SetValue(@"logLevel", logLevel.Value.ToString(CultureInfo.InvariantCulture));
        }

        private void SaveTextureSettings(IniEditor editor)
        {
            editor.SetValue(@"enableTextureDumping", enableTextureDumping.Checked ? @"1" : @"0");
            editor.SetValue(@"enableTextureOverride", enableTextureOverride.Checked ? @"1" : @"0");
        }

        private void SaveBackupSettings(IniEditor editor)
        {
            editor.SetValue(@"enableBackups", enableBackups.Checked ? @"1" : @"0");
            if (timeUnit.SelectedIndex == 0)
            {
                editor.SetValue(@"backupInterval", backupInterval.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                var backInt = Convert.ToString(backupInterval.Value * 60, CultureInfo.InvariantCulture);
                editor.SetValue(@"backupInterval", backInt);
            }
            editor.SetValue(@"maxBackups", maxBackups.Value.ToString(CultureInfo.InvariantCulture));
        }

        private void SaveWindowAndMouseSettings(IniEditor editor)
        {
            editor.SetValue(@"borderlessFullscreen", borderlessFullscreen.Checked ? @"1" : @"0");
            editor.SetValue(@"disableCursor", disableCursor.Checked ? @"1" : @"0");
            editor.SetValue(@"captureCursor", captureCursor.Checked ? @"1" : @"0");
        }

        private void SaveHudSettings(IniEditor editor)
        {
            editor.SetValue(@"enableHudMod", enableHudMod.Checked ? @"1" : @"0");
            editor.SetValue(@"enableMinimalHud", minimalHud.Checked ? @"1" : @"0");
            var scaled100 = hudScaleFactor.Value / 100.0;
            editor.SetValue(@"hudScaleFactor", scaled100.ToString(CultureInfo.InvariantCulture));
            scaled100 = topLeftCorner.Value / 100.0;
            editor.SetValue(@"hudTopLeftOpacity", scaled100.ToString(CultureInfo.InvariantCulture));
            scaled100 = bottomLeftCorner.Value / 100.0;
            editor.SetValue(@"hudBottomLeftOpacity", scaled100.ToString(CultureInfo.InvariantCulture));
            scaled100 = bottomRightCorner.Value / 100.0;
            editor.SetValue(@"hudBottomRightOpacity", scaled100.ToString(CultureInfo.InvariantCulture));
        }

        private void SaveGraphicsSettings(IniEditor editor)
        {
            var resolutionSplit = resolutionDrop.SelectedItem.ToString().Split('x');
            editor.SetValue(@"renderWidth", resolutionSplit[0]);
            editor.SetValue(@"renderHeight", resolutionSplit[1]);

            if (dofOverrideRes.Visible)
            {
                // "360" is a special case - it needs to say 0
                editor.SetValue(@"dofOverrideResolution", dofOverrideRes.Text != @"360" ? dofOverrideRes.Text : @"0");
            }
            else
            {
                editor.SetValue(@"dofOverrideResolution", dofOverrideResNum.Value != 360
                    ? dofOverrideResNum.Value.ToString(CultureInfo.InvariantCulture)
                    : @"0");
            }

            // I should've stuck with the "disable" part, but I didn't want unnecessary confusion.
            editor.SetValue(@"disableDofScaling", enableDofScaling.Checked ? @"0" : @"1");

            editor.SetValue(@"dofBlurAmount", dofBlurAmount.Value.ToString(CultureInfo.InvariantCulture));

            editor.SetValue(@"unlockFPS", unlockFPS.Checked ? @"1" : @"0");

            // After newest DSfix it has a "enabled?" thing
            editor.SetValue(@"FPSlimit", maxFPS.Value.ToString(CultureInfo.InvariantCulture));

            // FPS threshold
            // ReSharper disable once StringLiteralTypo
            editor.SetValue(@"FPSthreshold", FPSthreshold.Value.ToString(CultureInfo.InvariantCulture));

            editor.SetValue(@"presentWidth", presentWidth.Value.ToString(CultureInfo.InvariantCulture));
            editor.SetValue(@"presentHeight", presentHeight.Value.ToString(CultureInfo.InvariantCulture));
        }

        private void SaveAaAoSettings(IniEditor editor)
        {
            editor.SetValue(@"aaQuality", Convert.ToString(aaQuality.SelectedIndex, CultureInfo.CurrentCulture));
            editor.SetValue(@"ssaoStrength", Convert.ToString(ssaoStrength.SelectedIndex, CultureInfo.CurrentCulture));

            editor.SetValue(@"aaType", aaType.SelectedItem.ToString());
            editor.SetValue(@"ssaoScale", Convert.ToString(ssaoScale.SelectedIndex + 1, CultureInfo.CurrentCulture));
            editor.SetValue(@"ssaoType", ssaoType.SelectedItem.ToString());
        }

        private void appSaveButton_Click(object sender, EventArgs e)
        {
            if (!File.Exists(_appIni))
            {
                try
                {
                    File.Create(_appIni).Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        string.Format(
                            Resources.createSettingsIniError,
                            exception.Message), Resources.error);
                    return;
                }
            }

            SaveAll(_appSettingsEditor);

            // Keybindings
            SaveKeys(_appSettingsEditor);
        }

        #endregion Saving

        private void appLoadButton_Click(object sender, EventArgs e)
        {
            _isLoading = true;

            if (!File.Exists(_appIni))
            {
                MessageBox.Show(Resources.failedLoadSettingsIni);
                return;
            }

            _appSettingsEditor = new IniEditor(_appIni);
            LoadAll(_appSettingsEditor);
            LoadKeybindSettings(_appSettingsEditor);

            _isLoading = false;
        }

        private void timeUnit_Click(object sender, EventArgs e)
        {
            _timeUnitSelectedIndex = timeUnit.SelectedIndex;
        }

        private void timeUnit_KeyDown(object sender, KeyEventArgs e)
        {
            _timeUnitSelectedIndex = timeUnit.SelectedIndex;
        }

        /// <summary>
        /// Application-specific filtering/"renaming" of keys.
        /// </summary>
        /// <param name="e">The key event</param>
        /// <returns>A string which indicates what key was pressed. Names are specific to Dsfix's key options.</returns>
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        private static string FilterKey(KeyEventArgs e)
        {
            string r;

            if (e.KeyCode.ToString() == @"ControlKey")
            {
                r = @"RCONTROL";
            }
            else if (e.KeyCode.ToString() == @"ShiftKey")
            {
                r = @"RSHIFT";
            }
            else if (Regex.Match(e.KeyCode.ToString(), @"D(\d)").Success)
            {
                r = e.KeyCode.ToString().Remove(0, 1);
            }
            else
            {
                r = e.KeyCode.ToString();
            }

            return r;
        }

        private void keyBindings_KeyUp(object sender, KeyEventArgs e)
        {
            ((TextBox)sender).Text = FilterKey(e);
        }

        private void customFps_CheckedChanged(object sender, EventArgs e)
        {
            if (unlockFPS.Checked)
            {
                if (!_isLoading)
                {
                    MessageBox.Show(Resources.unlockedFpsWarning);
                }
                maxFPS.Enabled = true;
            }
            else if (!unlockFPS.Checked)
            {
                maxFPS.Enabled = false;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var info = new FileInfo(_path);
            Debug.Assert(info.DirectoryName != null, @"directory != null");
            var executablePath = Path.Combine(info.DirectoryName, "DARKSOULS.exe");
            if (File.Exists(executablePath))
            {
                Process.Start(executablePath);
            }
            else
            {
                MessageBox.Show(string.Format(Resources.failedToLocate, @"DARKSOULS.exe"));
            }
        }

        private static void CheckAntiAliasingSetting()
        {
            var gameSettingsPath = @"C:\Users\" + Environment.UserName +
                                   @"\AppData\Local\NBGI\DarkSouls\DarkSouls.ini";
            if (File.Exists(gameSettingsPath))
            {
                string readAll;
                using (TextReader reader = new StreamReader(gameSettingsPath))
                {
                    readAll = reader.ReadToEnd();
                }

                if (readAll.Contains(@"Antialiasing = 1"))
                {
                    if (MessageBox.Show(Resources.antialiasingEnabledPrompt, Resources.antialiasingEnabledPromptTitle,
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        readAll = readAll.Replace(@"Antialiasing = 1", @"Antialiasing = 0");

                        using (TextWriter writer = new StreamWriter(gameSettingsPath))
                        {
                            writer.Write(readAll);
                        }
                    }
                }
            }
        }

        private void dsmFixGuiButton_Click(object sender, EventArgs e)
        {
            var dsDirectory = Path.GetDirectoryName(_path);
            if (dsDirectory == null)
            {
                return;
            }
            var dsmFixExe = Path.Combine(dsDirectory, "dsmfixgui.exe");
            var startInfo = new ProcessStartInfo(dsmFixExe) { WorkingDirectory = dsDirectory };
            Process.Start(startInfo);
        }

        private void CheckMouseFix()
        {
            var dsPath = Path.GetDirectoryName(_path);
            if (dsPath == null)
            {
                return;
            }

            var mouseFixPath = Path.Combine(dsPath, "dsmfix.dll");
            var mouseFixExe = Path.Combine(dsPath, "dsmfixgui.exe");
            if (!File.Exists(mouseFixPath))
            {
                return;
            }

            if (File.Exists(mouseFixExe))
            {
                dsmFixGuiButton.Visible = true;
                dsmFixGuiLabel.Visible = true;
            }
            if (dinput8dllWrapper.Text == "" ||
                dinput8dllWrapper.Text.ToLower(CultureInfo.CurrentCulture) == @"none")
            {
                if (
                    MessageBox.Show(Resources.dsMouseFixDetectedNotAdded,
                        @"Mouse Fix",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    dinput8dllWrapper.Text = @"dsmfix.dll";
                    dsFixSaveButton.PerformClick();
                }
            }
        }
    }
}