﻿namespace DSFix_Tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clear up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.graphicOptions = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dofBlurAmount = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.dofOverrideResNum = new System.Windows.Forms.NumericUpDown();
            this.customDofOverrideRes = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.enableDofScaling = new System.Windows.Forms.CheckBox();
            this.dofOverrideRes = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.presentHeight = new System.Windows.Forms.NumericUpDown();
            this.presentWidth = new System.Windows.Forms.NumericUpDown();
            this.FPSthreshold = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.maxFPS = new System.Windows.Forms.NumericUpDown();
            this.unlockFPS = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.resolutionDrop = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.ssaoScale = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.ssaoType = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.ssaoStrength = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.aaType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.aaQuality = new System.Windows.Forms.ComboBox();
            this.hudOptions = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.bottomRightCorner = new System.Windows.Forms.TrackBar();
            this.topLeftCorner = new System.Windows.Forms.TrackBar();
            this.bottomLeftCorner = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.hudScaleFactor = new System.Windows.Forms.TrackBar();
            this.minimalHud = new System.Windows.Forms.CheckBox();
            this.enableHudMod = new System.Windows.Forms.CheckBox();
            this.windowMouseOptions = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.borderlessFullscreen = new System.Windows.Forms.CheckBox();
            this.captureCursor = new System.Windows.Forms.CheckBox();
            this.disableCursor = new System.Windows.Forms.CheckBox();
            this.saveBackupOptions = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.timeUnit = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.maxBackups = new System.Windows.Forms.NumericUpDown();
            this.backupInterval = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.enableBackups = new System.Windows.Forms.CheckBox();
            this.textureOverrideOptions = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.enableTextureOverride = new System.Windows.Forms.CheckBox();
            this.enableTextureDumping = new System.Windows.Forms.CheckBox();
            this.miscOptions = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dsmFixGuiButton = new System.Windows.Forms.Button();
            this.dsmFixGuiLabel = new System.Windows.Forms.Label();
            this.logLevel = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.dinput8dllWrapper = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.overrideLanguage = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.screenshotDir = new System.Windows.Forms.TextBox();
            this.skipIntro = new System.Windows.Forms.CheckBox();
            this.keyBindTab = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.keyPanel = new System.Windows.Forms.Panel();
            this.enabletoggle30FPSLimit = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.keytoggle30FPSLimit = new System.Windows.Forms.TextBox();
            this.enablereloadSSAOEffect = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.keyreloadSSAOEffect = new System.Windows.Forms.TextBox();
            this.enabletoggleHudChange = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.keytoggleHudChange = new System.Windows.Forms.TextBox();
            this.enabletoggleDofGauss = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.keytoggleDofGauss = new System.Windows.Forms.TextBox();
            this.enabletoggleVSSAO = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.keytoggleVSSAO = new System.Windows.Forms.TextBox();
            this.enabletoggleSMAA = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.keytoggleSMAA = new System.Windows.Forms.TextBox();
            this.enabletoggleHUD = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.keytoggleHUD = new System.Windows.Forms.TextBox();
            this.enabletakeHudlessScreenshot = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.keytakeHudlessScreenshot = new System.Windows.Forms.TextBox();
            this.enabletoggleBorderlessFullscreen = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.keytoggleBorderlessFullscreen = new System.Windows.Forms.TextBox();
            this.enabletoggleCursorVisibility = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.keytoggleCursorVisibility = new System.Windows.Forms.TextBox();
            this.enabletoggleCursorCapture = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.keytoggleCursorCapture = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gameLaunchButton = new System.Windows.Forms.Button();
            this.applicationLoadButton = new System.Windows.Forms.Button();
            this.applicationSaveButton = new System.Windows.Forms.Button();
            this.dsFixLoadButton = new System.Windows.Forms.Button();
            this.dsFixSaveButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.graphicOptions.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dofBlurAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dofOverrideResNum)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.presentHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.presentWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FPSthreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxFPS)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.hudOptions.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomRightCorner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topLeftCorner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomLeftCorner)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hudScaleFactor)).BeginInit();
            this.windowMouseOptions.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.saveBackupOptions.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxBackups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backupInterval)).BeginInit();
            this.textureOverrideOptions.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.miscOptions.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logLevel)).BeginInit();
            this.keyBindTab.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.keyPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.graphicOptions);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.hudOptions);
            this.tabControl1.Controls.Add(this.windowMouseOptions);
            this.tabControl1.Controls.Add(this.saveBackupOptions);
            this.tabControl1.Controls.Add(this.textureOverrideOptions);
            this.tabControl1.Controls.Add(this.miscOptions);
            this.tabControl1.Controls.Add(this.keyBindTab);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(565, 343);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.TabStop = false;
            // 
            // graphicOptions
            // 
            this.graphicOptions.Controls.Add(this.groupBox3);
            this.graphicOptions.Controls.Add(this.groupBox1);
            this.graphicOptions.Location = new System.Drawing.Point(4, 22);
            this.graphicOptions.Name = "graphicOptions";
            this.graphicOptions.Padding = new System.Windows.Forms.Padding(3);
            this.graphicOptions.Size = new System.Drawing.Size(557, 317);
            this.graphicOptions.TabIndex = 0;
            this.graphicOptions.Text = "Graphics";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dofBlurAmount);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.dofOverrideResNum);
            this.groupBox3.Controls.Add(this.customDofOverrideRes);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.enableDofScaling);
            this.groupBox3.Controls.Add(this.dofOverrideRes);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 170);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(551, 144);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Depth of Field (DoF) Settings";
            // 
            // dofBlurAmount
            // 
            this.dofBlurAmount.Location = new System.Drawing.Point(427, 73);
            this.dofBlurAmount.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.dofBlurAmount.Name = "dofBlurAmount";
            this.dofBlurAmount.Size = new System.Drawing.Size(121, 20);
            this.dofBlurAmount.TabIndex = 20;
            this.toolTip.SetToolTip(this.dofBlurAmount, "0 (off) at default DoF resolution.\r\n0 or 1 at 540 DoF resolution.\r\n1 or 2 at abov" +
        "e 540.\r\n3 or 4 at 2160 DoF resolution (if you\'re running a 680+.)\r\nPerformance d" +
        "egrades as blur amount increases.");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "DoF blur amount";
            // 
            // dofOverrideResNum
            // 
            this.dofOverrideResNum.Location = new System.Drawing.Point(427, 27);
            this.dofOverrideResNum.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.dofOverrideResNum.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.dofOverrideResNum.Name = "dofOverrideResNum";
            this.dofOverrideResNum.Size = new System.Drawing.Size(121, 20);
            this.dofOverrideResNum.TabIndex = 18;
            this.toolTip.SetToolTip(this.dofOverrideResNum, "Blurs the background to create the appearence of focus.");
            this.dofOverrideResNum.Value = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.dofOverrideResNum.Visible = false;
            // 
            // customDofOverrideRes
            // 
            this.customDofOverrideRes.AutoSize = true;
            this.customDofOverrideRes.Location = new System.Drawing.Point(360, 28);
            this.customDofOverrideRes.Name = "customDofOverrideRes";
            this.customDofOverrideRes.Size = new System.Drawing.Size(61, 17);
            this.customDofOverrideRes.TabIndex = 17;
            this.customDofOverrideRes.Text = "Custom";
            this.toolTip.SetToolTip(this.customDofOverrideRes, "Choose custom if you need to set a higher value than provided in the dropbox (unl" +
        "ikely.)");
            this.customDofOverrideRes.UseVisualStyleBackColor = true;
            this.customDofOverrideRes.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Enable DoF scaling (recommended)";
            // 
            // enableDofScaling
            // 
            this.enableDofScaling.AutoSize = true;
            this.enableDofScaling.Checked = true;
            this.enableDofScaling.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableDofScaling.Location = new System.Drawing.Point(427, 53);
            this.enableDofScaling.Name = "enableDofScaling";
            this.enableDofScaling.Size = new System.Drawing.Size(15, 14);
            this.enableDofScaling.TabIndex = 2;
            this.toolTip.SetToolTip(this.enableDofScaling, "Recommended to be turned on, standard in the game.\r\nTurning off decreases perform" +
        "ance, but creates a sharper image.");
            this.enableDofScaling.UseVisualStyleBackColor = true;
            // 
            // dofOverrideRes
            // 
            this.dofOverrideRes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dofOverrideRes.FormattingEnabled = true;
            this.dofOverrideRes.Items.AddRange(new object[] {
            "360",
            "540",
            "810",
            "1080",
            "2160"});
            this.dofOverrideRes.Location = new System.Drawing.Point(427, 26);
            this.dofOverrideRes.Name = "dofOverrideRes";
            this.dofOverrideRes.Size = new System.Drawing.Size(121, 21);
            this.dofOverrideRes.TabIndex = 1;
            this.toolTip.SetToolTip(this.dofOverrideRes, resources.GetString("dofOverrideRes.ToolTip"));
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Depth of Field override resolution";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.presentHeight);
            this.groupBox1.Controls.Add(this.presentWidth);
            this.groupBox1.Controls.Add(this.FPSthreshold);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.maxFPS);
            this.groupBox1.Controls.Add(this.unlockFPS);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.resolutionDrop);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(551, 167);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Render Settings";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(7, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 13);
            this.label42.TabIndex = 47;
            this.label42.Text = "Present Width";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(7, 72);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(77, 13);
            this.label43.TabIndex = 48;
            this.label43.Text = "Present Height";
            this.toolTip.SetToolTip(this.label43, "\r\n");
            // 
            // presentHeight
            // 
            this.presentHeight.Location = new System.Drawing.Point(427, 70);
            this.presentHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.presentHeight.Name = "presentHeight";
            this.presentHeight.Size = new System.Drawing.Size(121, 20);
            this.presentHeight.TabIndex = 46;
            this.toolTip.SetToolTip(this.presentHeight, "Resolution for downscaling\r\nIf 0: Use the same as rendering resolution\r\nIf in dou" +
        "bt leave it at 0");
            // 
            // presentWidth
            // 
            this.presentWidth.AccessibleDescription = "";
            this.presentWidth.Location = new System.Drawing.Point(427, 44);
            this.presentWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.presentWidth.Name = "presentWidth";
            this.presentWidth.Size = new System.Drawing.Size(121, 20);
            this.presentWidth.TabIndex = 45;
            this.toolTip.SetToolTip(this.presentWidth, "Resolution for downscaling\r\nIf 0: Use the same as rendering resolution\r\nIf in dou" +
        "bt leave it at 0");
            // 
            // FPSthreshold
            // 
            this.FPSthreshold.Location = new System.Drawing.Point(427, 134);
            this.FPSthreshold.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.FPSthreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FPSthreshold.Name = "FPSthreshold";
            this.FPSthreshold.Size = new System.Drawing.Size(121, 20);
            this.FPSthreshold.TabIndex = 44;
            this.toolTip.SetToolTip(this.FPSthreshold, "DSfix will dynamically disable anti-aliasing if your framerate drops below this v" +
        "alue  and re-enable it once it has normalized.");
            this.FPSthreshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 136);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(263, 13);
            this.label38.TabIndex = 43;
            this.label38.Text = "Dynamically disable AA if your framerate drops below...";
            // 
            // maxFPS
            // 
            this.maxFPS.Enabled = false;
            this.maxFPS.Location = new System.Drawing.Point(427, 108);
            this.maxFPS.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.maxFPS.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.maxFPS.Name = "maxFPS";
            this.maxFPS.Size = new System.Drawing.Size(121, 20);
            this.maxFPS.TabIndex = 42;
            this.maxFPS.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // unlockFPS
            // 
            this.unlockFPS.AutoSize = true;
            this.unlockFPS.Location = new System.Drawing.Point(360, 109);
            this.unlockFPS.Name = "unlockFPS";
            this.unlockFPS.Size = new System.Drawing.Size(60, 17);
            this.unlockFPS.TabIndex = 41;
            this.unlockFPS.Text = "Unlock";
            this.toolTip.SetToolTip(this.unlockFPS, resources.GetString("unlockFPS.ToolTip"));
            this.unlockFPS.UseVisualStyleBackColor = true;
            this.unlockFPS.CheckedChanged += new System.EventHandler(this.customFps_CheckedChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 110);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 13);
            this.label37.TabIndex = 40;
            this.label37.Text = "Frames per second";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Rendering Resolution";
            // 
            // resolutionDrop
            // 
            this.resolutionDrop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionDrop.FormattingEnabled = true;
            this.resolutionDrop.Location = new System.Drawing.Point(359, 17);
            this.resolutionDrop.Name = "resolutionDrop";
            this.resolutionDrop.Size = new System.Drawing.Size(188, 21);
            this.resolutionDrop.TabIndex = 38;
            this.toolTip.SetToolTip(this.resolutionDrop, "Allows you to select the resolution to render at.\r\n\r\nAdv.: For downscaling from a" +
        "rbitrary resolution, edit these two lines in the file manually.");
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(215, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(134, 23);
            this.button3.TabIndex = 34;
            this.button3.Text = "Set to desktop resolution";
            this.toolTip.SetToolTip(this.button3, "Automatically sets render width and height to match your primary monitor\'s resolu" +
        "tion.");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(557, 317);
            this.tabPage1.TabIndex = 7;
            this.tabPage1.Text = "AA & SSAO";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.ssaoScale);
            this.groupBox11.Controls.Add(this.label41);
            this.groupBox11.Controls.Add(this.ssaoType);
            this.groupBox11.Controls.Add(this.label40);
            this.groupBox11.Controls.Add(this.ssaoStrength);
            this.groupBox11.Controls.Add(this.label5);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Location = new System.Drawing.Point(0, 103);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(557, 214);
            this.groupBox11.TabIndex = 21;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "AO Settings";
            // 
            // ssaoScale
            // 
            this.ssaoScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ssaoScale.FormattingEnabled = true;
            this.ssaoScale.Items.AddRange(new object[] {
            "High quality",
            "Lower quality",
            "Lowest quality"});
            this.ssaoScale.Location = new System.Drawing.Point(428, 72);
            this.ssaoScale.Name = "ssaoScale";
            this.ssaoScale.Size = new System.Drawing.Size(121, 21);
            this.ssaoScale.TabIndex = 19;
            this.toolTip.SetToolTip(this.ssaoScale, "Lower quality = lower impact on performance");
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(8, 75);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(64, 13);
            this.label41.TabIndex = 20;
            this.label41.Text = "SSAO scale";
            // 
            // ssaoType
            // 
            this.ssaoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ssaoType.FormattingEnabled = true;
            this.ssaoType.Items.AddRange(new object[] {
            "VSSAO",
            "HBAO",
            "SCAO",
            "VSSAO2"});
            this.ssaoType.Location = new System.Drawing.Point(428, 18);
            this.ssaoType.Name = "ssaoType";
            this.ssaoType.Size = new System.Drawing.Size(121, 21);
            this.ssaoType.TabIndex = 18;
            this.toolTip.SetToolTip(this.ssaoType, resources.GetString("ssaoType.ToolTip"));
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(8, 21);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(59, 13);
            this.label40.TabIndex = 17;
            this.label40.Text = "SSAO type";
            // 
            // ssaoStrength
            // 
            this.ssaoStrength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ssaoStrength.FormattingEnabled = true;
            this.ssaoStrength.Items.AddRange(new object[] {
            "Off",
            "Low",
            "Medium",
            "Strong"});
            this.ssaoStrength.Location = new System.Drawing.Point(428, 45);
            this.ssaoStrength.Name = "ssaoStrength";
            this.ssaoStrength.Size = new System.Drawing.Size(121, 21);
            this.ssaoStrength.TabIndex = 15;
            this.toolTip.SetToolTip(this.ssaoStrength, "Applies to create the appearence of real lighting.\r\nAnything above \"off\" has the " +
        "same negative impact on performance.\r\nMeaning whether you choose \"Low\" or \"Stron" +
        "g\" it will have the same impact.");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "SSAO strength";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.aaType);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.aaQuality);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(557, 103);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AA Settings";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 13);
            this.label39.TabIndex = 18;
            this.label39.Text = "AA type";
            // 
            // aaType
            // 
            this.aaType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aaType.FormattingEnabled = true;
            this.aaType.Items.AddRange(new object[] {
            "SMAA",
            "FXAA"});
            this.aaType.Location = new System.Drawing.Point(428, 19);
            this.aaType.Name = "aaType";
            this.aaType.Size = new System.Drawing.Size(121, 21);
            this.aaType.TabIndex = 17;
            this.toolTip.SetToolTip(this.aaType, "Antialiasing. Removes pixelated edges.\r\nFXAA is less resource intensive than SMAA" +
        ", but creates some blur.");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "AA quality";
            // 
            // aaQuality
            // 
            this.aaQuality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aaQuality.FormattingEnabled = true;
            this.aaQuality.Items.AddRange(new object[] {
            "Off",
            "Low",
            "Medium",
            "High",
            "Ultra"});
            this.aaQuality.Location = new System.Drawing.Point(428, 46);
            this.aaQuality.Name = "aaQuality";
            this.aaQuality.Size = new System.Drawing.Size(121, 21);
            this.aaQuality.TabIndex = 14;
            this.toolTip.SetToolTip(this.aaQuality, "Antialiasing. Removes pixelated edges.\r\nPerformance degrades corresponding to set" +
        "ting.\r\nTry lowering this setting if you get frequent framedrops.");
            // 
            // hudOptions
            // 
            this.hudOptions.Controls.Add(this.groupBox5);
            this.hudOptions.Controls.Add(this.groupBox4);
            this.hudOptions.Location = new System.Drawing.Point(4, 22);
            this.hudOptions.Name = "hudOptions";
            this.hudOptions.Padding = new System.Windows.Forms.Padding(3);
            this.hudOptions.Size = new System.Drawing.Size(557, 317);
            this.hudOptions.TabIndex = 1;
            this.hudOptions.Text = "HUD";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.bottomRightCorner);
            this.groupBox5.Controls.Add(this.topLeftCorner);
            this.groupBox5.Controls.Add(this.bottomLeftCorner);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 121);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(551, 193);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Opacity";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Top left corner";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 82);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Bottom left corner";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Bottom right corner";
            // 
            // bottomRightCorner
            // 
            this.bottomRightCorner.Enabled = false;
            this.bottomRightCorner.Location = new System.Drawing.Point(110, 116);
            this.bottomRightCorner.Maximum = 100;
            this.bottomRightCorner.Name = "bottomRightCorner";
            this.bottomRightCorner.Size = new System.Drawing.Size(397, 45);
            this.bottomRightCorner.TabIndex = 16;
            this.toolTip.SetToolTip(this.bottomRightCorner, "Transparency on the bottom right part of the HUD.\r\nBottom right: soul count.\r\n1.0" +
        " for fully visible.\r\n0.0 for invisible.");
            this.bottomRightCorner.ValueChanged += new System.EventHandler(this.trackBars_ValueChanged);
            // 
            // topLeftCorner
            // 
            this.topLeftCorner.Enabled = false;
            this.topLeftCorner.Location = new System.Drawing.Point(110, 16);
            this.topLeftCorner.Maximum = 100;
            this.topLeftCorner.Name = "topLeftCorner";
            this.topLeftCorner.Size = new System.Drawing.Size(397, 45);
            this.topLeftCorner.TabIndex = 14;
            this.toolTip.SetToolTip(this.topLeftCorner, "Transparency on the top left part of the HUD.\r\nTop left: health bars, stamina bar" +
        ", humanity counter, status indicators\r\n1.0 for fully visible.\r\n0.0 for invisible" +
        ".");
            this.topLeftCorner.ValueChanged += new System.EventHandler(this.trackBars_ValueChanged);
            // 
            // bottomLeftCorner
            // 
            this.bottomLeftCorner.Enabled = false;
            this.bottomLeftCorner.Location = new System.Drawing.Point(110, 65);
            this.bottomLeftCorner.Maximum = 100;
            this.bottomLeftCorner.Name = "bottomLeftCorner";
            this.bottomLeftCorner.Size = new System.Drawing.Size(397, 45);
            this.bottomLeftCorner.TabIndex = 15;
            this.toolTip.SetToolTip(this.bottomLeftCorner, "Transparency on the bottom left part of the HUD.\r\nBottom left: item indicators & " +
        "counts.\r\n1.0 for fully visible.\r\n0.0 for invisible.\r\n");
            this.bottomLeftCorner.ValueChanged += new System.EventHandler(this.trackBars_ValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.hudScaleFactor);
            this.groupBox4.Controls.Add(this.minimalHud);
            this.groupBox4.Controls.Add(this.enableHudMod);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(551, 118);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "HUD settings";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "HUD scale factor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "0";
            // 
            // hudScaleFactor
            // 
            this.hudScaleFactor.Enabled = false;
            this.hudScaleFactor.Location = new System.Drawing.Point(110, 70);
            this.hudScaleFactor.Maximum = 100;
            this.hudScaleFactor.Name = "hudScaleFactor";
            this.hudScaleFactor.Size = new System.Drawing.Size(397, 45);
            this.hudScaleFactor.TabIndex = 19;
            this.toolTip.SetToolTip(this.hudScaleFactor, "Scale down the HUD.");
            this.hudScaleFactor.ValueChanged += new System.EventHandler(this.trackBars_ValueChanged);
            // 
            // minimalHud
            // 
            this.minimalHud.AutoSize = true;
            this.minimalHud.Enabled = false;
            this.minimalHud.Location = new System.Drawing.Point(9, 40);
            this.minimalHud.Name = "minimalHud";
            this.minimalHud.Size = new System.Drawing.Size(88, 17);
            this.minimalHud.TabIndex = 2;
            this.minimalHud.Text = "Minimal HUD";
            this.toolTip.SetToolTip(this.minimalHud, "Removes left- and right- hand items in the lower-left corner.");
            this.minimalHud.UseVisualStyleBackColor = true;
            // 
            // enableHudMod
            // 
            this.enableHudMod.AutoSize = true;
            this.enableHudMod.Location = new System.Drawing.Point(9, 17);
            this.enableHudMod.Name = "enableHudMod";
            this.enableHudMod.Size = new System.Drawing.Size(150, 17);
            this.enableHudMod.TabIndex = 1;
            this.enableHudMod.Text = "Enable HUD modifications";
            this.enableHudMod.UseVisualStyleBackColor = true;
            this.enableHudMod.CheckedChanged += new System.EventHandler(this.enableHudMod_CheckedChanged);
            // 
            // windowMouseOptions
            // 
            this.windowMouseOptions.Controls.Add(this.groupBox6);
            this.windowMouseOptions.Location = new System.Drawing.Point(4, 22);
            this.windowMouseOptions.Name = "windowMouseOptions";
            this.windowMouseOptions.Size = new System.Drawing.Size(557, 317);
            this.windowMouseOptions.TabIndex = 2;
            this.windowMouseOptions.Text = "Windows & Mouse";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.borderlessFullscreen);
            this.groupBox6.Controls.Add(this.captureCursor);
            this.groupBox6.Controls.Add(this.disableCursor);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(557, 317);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            // 
            // borderlessFullscreen
            // 
            this.borderlessFullscreen.AutoSize = true;
            this.borderlessFullscreen.Location = new System.Drawing.Point(6, 20);
            this.borderlessFullscreen.Name = "borderlessFullscreen";
            this.borderlessFullscreen.Size = new System.Drawing.Size(123, 17);
            this.borderlessFullscreen.TabIndex = 0;
            this.borderlessFullscreen.Text = "Borderless fullscreen";
            this.toolTip.SetToolTip(this.borderlessFullscreen, resources.GetString("borderlessFullscreen.ToolTip"));
            this.borderlessFullscreen.UseVisualStyleBackColor = true;
            // 
            // captureCursor
            // 
            this.captureCursor.AutoSize = true;
            this.captureCursor.Location = new System.Drawing.Point(6, 66);
            this.captureCursor.Name = "captureCursor";
            this.captureCursor.Size = new System.Drawing.Size(95, 17);
            this.captureCursor.TabIndex = 2;
            this.captureCursor.Text = "Capture cursor";
            this.toolTip.SetToolTip(this.captureCursor, "Doesn\'t allow the cursor to leave the game window.\r\nRecommended on dual-screen.");
            this.captureCursor.UseVisualStyleBackColor = true;
            // 
            // disableCursor
            // 
            this.disableCursor.AutoSize = true;
            this.disableCursor.Location = new System.Drawing.Point(6, 43);
            this.disableCursor.Name = "disableCursor";
            this.disableCursor.Size = new System.Drawing.Size(93, 17);
            this.disableCursor.TabIndex = 1;
            this.disableCursor.Text = "Disable cursor";
            this.disableCursor.UseVisualStyleBackColor = true;
            // 
            // saveBackupOptions
            // 
            this.saveBackupOptions.Controls.Add(this.groupBox7);
            this.saveBackupOptions.Location = new System.Drawing.Point(4, 22);
            this.saveBackupOptions.Name = "saveBackupOptions";
            this.saveBackupOptions.Size = new System.Drawing.Size(557, 317);
            this.saveBackupOptions.TabIndex = 3;
            this.saveBackupOptions.Text = "Save Backup";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.timeUnit);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.maxBackups);
            this.groupBox7.Controls.Add(this.backupInterval);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.enableBackups);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(557, 317);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            // 
            // timeUnit
            // 
            this.timeUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeUnit.FormattingEnabled = true;
            this.timeUnit.Items.AddRange(new object[] {
            "seconds",
            "minutes"});
            this.timeUnit.Location = new System.Drawing.Point(399, 38);
            this.timeUnit.Name = "timeUnit";
            this.timeUnit.Size = new System.Drawing.Size(121, 21);
            this.timeUnit.TabIndex = 5;
            this.toolTip.SetToolTip(this.timeUnit, "Show backup interval in minutes or seconds?");
            this.timeUnit.SelectedIndexChanged += new System.EventHandler(this.timeUnit_SelectedIndexChanged);
            this.timeUnit.Click += new System.EventHandler(this.timeUnit_Click);
            this.timeUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.timeUnit_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Enable backups";
            // 
            // maxBackups
            // 
            this.maxBackups.Location = new System.Drawing.Point(273, 65);
            this.maxBackups.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.maxBackups.Name = "maxBackups";
            this.maxBackups.Size = new System.Drawing.Size(120, 20);
            this.maxBackups.TabIndex = 4;
            this.toolTip.SetToolTip(this.maxBackups, "How many backups will DsFix create before replacing old ones?");
            this.maxBackups.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // backupInterval
            // 
            this.backupInterval.Location = new System.Drawing.Point(273, 39);
            this.backupInterval.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.backupInterval.Minimum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.backupInterval.Name = "backupInterval";
            this.backupInterval.Size = new System.Drawing.Size(120, 20);
            this.backupInterval.TabIndex = 3;
            this.toolTip.SetToolTip(this.backupInterval, "At what interval will DsFix make a backup of your current save?");
            this.backupInterval.Value = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 67);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(150, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Maximum amounts of backups";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Backup interval";
            // 
            // enableBackups
            // 
            this.enableBackups.AutoSize = true;
            this.enableBackups.Location = new System.Drawing.Point(273, 19);
            this.enableBackups.Name = "enableBackups";
            this.enableBackups.Size = new System.Drawing.Size(15, 14);
            this.enableBackups.TabIndex = 0;
            this.enableBackups.UseVisualStyleBackColor = true;
            // 
            // textureOverrideOptions
            // 
            this.textureOverrideOptions.Controls.Add(this.groupBox8);
            this.textureOverrideOptions.Location = new System.Drawing.Point(4, 22);
            this.textureOverrideOptions.Name = "textureOverrideOptions";
            this.textureOverrideOptions.Size = new System.Drawing.Size(557, 317);
            this.textureOverrideOptions.TabIndex = 4;
            this.textureOverrideOptions.Text = "Texture Override";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.enableTextureOverride);
            this.groupBox8.Controls.Add(this.enableTextureDumping);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(0, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(557, 317);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            // 
            // enableTextureOverride
            // 
            this.enableTextureOverride.AutoSize = true;
            this.enableTextureOverride.Location = new System.Drawing.Point(8, 42);
            this.enableTextureOverride.Name = "enableTextureOverride";
            this.enableTextureOverride.Size = new System.Drawing.Size(135, 17);
            this.enableTextureOverride.TabIndex = 1;
            this.enableTextureOverride.Text = "Enable texture override";
            this.toolTip.SetToolTip(this.enableTextureOverride, "Enable to use custom textures.");
            this.enableTextureOverride.UseVisualStyleBackColor = true;
            // 
            // enableTextureDumping
            // 
            this.enableTextureDumping.AutoSize = true;
            this.enableTextureDumping.Location = new System.Drawing.Point(8, 19);
            this.enableTextureDumping.Name = "enableTextureDumping";
            this.enableTextureDumping.Size = new System.Drawing.Size(137, 17);
            this.enableTextureDumping.TabIndex = 0;
            this.enableTextureDumping.Text = "Enable texture dumping";
            this.toolTip.SetToolTip(this.enableTextureDumping, "Only enable this if you\'re interested in modifying textures.");
            this.enableTextureDumping.UseVisualStyleBackColor = true;
            // 
            // miscOptions
            // 
            this.miscOptions.Controls.Add(this.groupBox9);
            this.miscOptions.Location = new System.Drawing.Point(4, 22);
            this.miscOptions.Name = "miscOptions";
            this.miscOptions.Size = new System.Drawing.Size(557, 317);
            this.miscOptions.TabIndex = 5;
            this.miscOptions.Text = "Misc.";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dsmFixGuiButton);
            this.groupBox9.Controls.Add(this.dsmFixGuiLabel);
            this.groupBox9.Controls.Add(this.logLevel);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.dinput8dllWrapper);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.overrideLanguage);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.screenshotDir);
            this.groupBox9.Controls.Add(this.skipIntro);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(0, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(557, 317);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            // 
            // dsmFixGuiButton
            // 
            this.dsmFixGuiButton.Location = new System.Drawing.Point(435, 154);
            this.dsmFixGuiButton.Name = "dsmFixGuiButton";
            this.dsmFixGuiButton.Size = new System.Drawing.Size(75, 23);
            this.dsmFixGuiButton.TabIndex = 11;
            this.dsmFixGuiButton.Text = "Open";
            this.toolTip.SetToolTip(this.dsmFixGuiButton, "Since you have the mouse fix in the dark souls directory,\r\nthen you can use this " +
        "button to easily launch it\r\nwithout having to navigate to it.");
            this.dsmFixGuiButton.UseVisualStyleBackColor = true;
            this.dsmFixGuiButton.Visible = false;
            this.dsmFixGuiButton.Click += new System.EventHandler(this.dsmFixGuiButton_Click);
            // 
            // dsmFixGuiLabel
            // 
            this.dsmFixGuiLabel.AutoSize = true;
            this.dsmFixGuiLabel.Location = new System.Drawing.Point(5, 154);
            this.dsmFixGuiLabel.Name = "dsmFixGuiLabel";
            this.dsmFixGuiLabel.Size = new System.Drawing.Size(157, 13);
            this.dsmFixGuiLabel.TabIndex = 10;
            this.dsmFixGuiLabel.Text = "Open Dark Souls mouse fix GUI";
            this.dsmFixGuiLabel.Visible = false;
            // 
            // logLevel
            // 
            this.logLevel.Location = new System.Drawing.Point(390, 128);
            this.logLevel.Maximum = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.logLevel.Name = "logLevel";
            this.logLevel.Size = new System.Drawing.Size(120, 20);
            this.logLevel.TabIndex = 9;
            this.toolTip.SetToolTip(this.logLevel, "Only enable for debugging.\r\nHigher number means more logging.");
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 130);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "Log level";
            // 
            // dinput8dllWrapper
            // 
            this.dinput8dllWrapper.Location = new System.Drawing.Point(139, 102);
            this.dinput8dllWrapper.Name = "dinput8dllWrapper";
            this.dinput8dllWrapper.Size = new System.Drawing.Size(371, 20);
            this.dinput8dllWrapper.TabIndex = 7;
            this.dinput8dllWrapper.Text = "none";
            this.toolTip.SetToolTip(this.dinput8dllWrapper, "If you want to use another dinput8.dll wrapper together with DSfix, rename it (e." +
        "g. \"dinputwrapper.dll\") and put the new name here.");
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 105);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(125, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Dinput wrapper (optional)";
            // 
            // overrideLanguage
            // 
            this.overrideLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.overrideLanguage.FormattingEnabled = true;
            this.overrideLanguage.Items.AddRange(new object[] {
            "No override",
            "English",
            "French",
            "Italian",
            "German",
            "Spanish",
            "Korean",
            "Chinese",
            "Polish",
            "Russian"});
            this.overrideLanguage.Location = new System.Drawing.Point(389, 74);
            this.overrideLanguage.Name = "overrideLanguage";
            this.overrideLanguage.Size = new System.Drawing.Size(121, 21);
            this.overrideLanguage.TabIndex = 5;
            this.toolTip.SetToolTip(this.overrideLanguage, "Allows you to override the in-game language.");
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 77);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Override language";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Skip intro?";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(104, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Screenshot directory";
            // 
            // screenshotDir
            // 
            this.screenshotDir.Location = new System.Drawing.Point(139, 45);
            this.screenshotDir.Name = "screenshotDir";
            this.screenshotDir.Size = new System.Drawing.Size(371, 20);
            this.screenshotDir.TabIndex = 1;
            this.toolTip.SetToolTip(this.screenshotDir, "Example:\r\nC:\\Users\\Morten\\Pictures\\dark souls screenshots\r\nEnd without a \'\\\'.\r\n");
            // 
            // skipIntro
            // 
            this.skipIntro.AutoSize = true;
            this.skipIntro.Location = new System.Drawing.Point(495, 19);
            this.skipIntro.Name = "skipIntro";
            this.skipIntro.Size = new System.Drawing.Size(15, 14);
            this.skipIntro.TabIndex = 0;
            this.toolTip.SetToolTip(this.skipIntro, "Skip the intro?");
            this.skipIntro.UseVisualStyleBackColor = true;
            // 
            // keyBindTab
            // 
            this.keyBindTab.Controls.Add(this.groupBox10);
            this.keyBindTab.Location = new System.Drawing.Point(4, 22);
            this.keyBindTab.Name = "keyBindTab";
            this.keyBindTab.Size = new System.Drawing.Size(557, 317);
            this.keyBindTab.TabIndex = 6;
            this.keyBindTab.Text = "Keybindings";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.keyPanel);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Location = new System.Drawing.Point(0, 0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(557, 317);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            // 
            // keyPanel
            // 
            this.keyPanel.AutoScroll = true;
            this.keyPanel.Controls.Add(this.enabletoggle30FPSLimit);
            this.keyPanel.Controls.Add(this.label35);
            this.keyPanel.Controls.Add(this.keytoggle30FPSLimit);
            this.keyPanel.Controls.Add(this.enablereloadSSAOEffect);
            this.keyPanel.Controls.Add(this.label36);
            this.keyPanel.Controls.Add(this.keyreloadSSAOEffect);
            this.keyPanel.Controls.Add(this.enabletoggleHudChange);
            this.keyPanel.Controls.Add(this.label31);
            this.keyPanel.Controls.Add(this.keytoggleHudChange);
            this.keyPanel.Controls.Add(this.enabletoggleDofGauss);
            this.keyPanel.Controls.Add(this.label32);
            this.keyPanel.Controls.Add(this.keytoggleDofGauss);
            this.keyPanel.Controls.Add(this.enabletoggleVSSAO);
            this.keyPanel.Controls.Add(this.label33);
            this.keyPanel.Controls.Add(this.keytoggleVSSAO);
            this.keyPanel.Controls.Add(this.enabletoggleSMAA);
            this.keyPanel.Controls.Add(this.label28);
            this.keyPanel.Controls.Add(this.keytoggleSMAA);
            this.keyPanel.Controls.Add(this.enabletoggleHUD);
            this.keyPanel.Controls.Add(this.label29);
            this.keyPanel.Controls.Add(this.keytoggleHUD);
            this.keyPanel.Controls.Add(this.enabletakeHudlessScreenshot);
            this.keyPanel.Controls.Add(this.label30);
            this.keyPanel.Controls.Add(this.keytakeHudlessScreenshot);
            this.keyPanel.Controls.Add(this.enabletoggleBorderlessFullscreen);
            this.keyPanel.Controls.Add(this.label27);
            this.keyPanel.Controls.Add(this.keytoggleBorderlessFullscreen);
            this.keyPanel.Controls.Add(this.enabletoggleCursorVisibility);
            this.keyPanel.Controls.Add(this.label26);
            this.keyPanel.Controls.Add(this.keytoggleCursorVisibility);
            this.keyPanel.Controls.Add(this.enabletoggleCursorCapture);
            this.keyPanel.Controls.Add(this.label25);
            this.keyPanel.Controls.Add(this.keytoggleCursorCapture);
            this.keyPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyPanel.Location = new System.Drawing.Point(3, 16);
            this.keyPanel.Name = "keyPanel";
            this.keyPanel.Size = new System.Drawing.Size(551, 298);
            this.keyPanel.TabIndex = 0;
            // 
            // enabletoggle30FPSLimit
            // 
            this.enabletoggle30FPSLimit.AutoSize = true;
            this.enabletoggle30FPSLimit.Location = new System.Drawing.Point(367, 271);
            this.enabletoggle30FPSLimit.Name = "enabletoggle30FPSLimit";
            this.enabletoggle30FPSLimit.Size = new System.Drawing.Size(15, 14);
            this.enabletoggle30FPSLimit.TabIndex = 68;
            this.enabletoggle30FPSLimit.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggle30FPSLimit, "Enable/disable the keybinding.");
            this.enabletoggle30FPSLimit.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(5, 271);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 13);
            this.label35.TabIndex = 67;
            this.label35.Text = "Toggle FPS limit";
            // 
            // keytoggle30FPSLimit
            // 
            this.keytoggle30FPSLimit.Location = new System.Drawing.Point(388, 268);
            this.keytoggle30FPSLimit.Name = "keytoggle30FPSLimit";
            this.keytoggle30FPSLimit.ReadOnly = true;
            this.keytoggle30FPSLimit.Size = new System.Drawing.Size(100, 20);
            this.keytoggle30FPSLimit.TabIndex = 66;
            this.keytoggle30FPSLimit.TabStop = false;
            this.keytoggle30FPSLimit.Text = "Back";
            this.toolTip.SetToolTip(this.keytoggle30FPSLimit, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggle30FPSLimit.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enablereloadSSAOEffect
            // 
            this.enablereloadSSAOEffect.AutoSize = true;
            this.enablereloadSSAOEffect.Location = new System.Drawing.Point(367, 245);
            this.enablereloadSSAOEffect.Name = "enablereloadSSAOEffect";
            this.enablereloadSSAOEffect.Size = new System.Drawing.Size(15, 14);
            this.enablereloadSSAOEffect.TabIndex = 65;
            this.enablereloadSSAOEffect.TabStop = false;
            this.toolTip.SetToolTip(this.enablereloadSSAOEffect, "Enable/disable the keybinding.");
            this.enablereloadSSAOEffect.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(5, 245);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(103, 13);
            this.label36.TabIndex = 64;
            this.label36.Text = "Reload SSAO effect";
            // 
            // keyreloadSSAOEffect
            // 
            this.keyreloadSSAOEffect.Location = new System.Drawing.Point(388, 242);
            this.keyreloadSSAOEffect.Name = "keyreloadSSAOEffect";
            this.keyreloadSSAOEffect.ReadOnly = true;
            this.keyreloadSSAOEffect.Size = new System.Drawing.Size(100, 20);
            this.keyreloadSSAOEffect.TabIndex = 63;
            this.keyreloadSSAOEffect.TabStop = false;
            this.keyreloadSSAOEffect.Text = "NumPad5";
            this.toolTip.SetToolTip(this.keyreloadSSAOEffect, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keyreloadSSAOEffect.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleHudChange
            // 
            this.enabletoggleHudChange.AutoSize = true;
            this.enabletoggleHudChange.Location = new System.Drawing.Point(367, 219);
            this.enabletoggleHudChange.Name = "enabletoggleHudChange";
            this.enabletoggleHudChange.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleHudChange.TabIndex = 62;
            this.enabletoggleHudChange.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleHudChange, "Enable/disable the keybinding.");
            this.enabletoggleHudChange.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 219);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(106, 13);
            this.label31.TabIndex = 61;
            this.label31.Text = "Toggle HUD change";
            // 
            // keytoggleHudChange
            // 
            this.keytoggleHudChange.Location = new System.Drawing.Point(388, 216);
            this.keytoggleHudChange.Name = "keytoggleHudChange";
            this.keytoggleHudChange.ReadOnly = true;
            this.keytoggleHudChange.Size = new System.Drawing.Size(100, 20);
            this.keytoggleHudChange.TabIndex = 60;
            this.keytoggleHudChange.TabStop = false;
            this.keytoggleHudChange.Text = "RSHIFT";
            this.toolTip.SetToolTip(this.keytoggleHudChange, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleHudChange.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleDofGauss
            // 
            this.enabletoggleDofGauss.AutoSize = true;
            this.enabletoggleDofGauss.Location = new System.Drawing.Point(367, 193);
            this.enabletoggleDofGauss.Name = "enabletoggleDofGauss";
            this.enabletoggleDofGauss.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleDofGauss.TabIndex = 59;
            this.enabletoggleDofGauss.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleDofGauss, "Enable/disable the keybinding.");
            this.enabletoggleDofGauss.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(5, 193);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(96, 13);
            this.label32.TabIndex = 58;
            this.label32.Text = "Toggle DOF gauss";
            // 
            // keytoggleDofGauss
            // 
            this.keytoggleDofGauss.Location = new System.Drawing.Point(388, 190);
            this.keytoggleDofGauss.Name = "keytoggleDofGauss";
            this.keytoggleDofGauss.ReadOnly = true;
            this.keytoggleDofGauss.Size = new System.Drawing.Size(100, 20);
            this.keytoggleDofGauss.TabIndex = 57;
            this.keytoggleDofGauss.TabStop = false;
            this.keytoggleDofGauss.Text = "NumPad3";
            this.toolTip.SetToolTip(this.keytoggleDofGauss, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleDofGauss.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleVSSAO
            // 
            this.enabletoggleVSSAO.AutoSize = true;
            this.enabletoggleVSSAO.Location = new System.Drawing.Point(367, 167);
            this.enabletoggleVSSAO.Name = "enabletoggleVSSAO";
            this.enabletoggleVSSAO.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleVSSAO.TabIndex = 56;
            this.enabletoggleVSSAO.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleVSSAO, "Enable/disable the keybinding.");
            this.enabletoggleVSSAO.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(5, 167);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 13);
            this.label33.TabIndex = 55;
            this.label33.Text = "Toggle VSSAO";
            // 
            // keytoggleVSSAO
            // 
            this.keytoggleVSSAO.Location = new System.Drawing.Point(388, 164);
            this.keytoggleVSSAO.Name = "keytoggleVSSAO";
            this.keytoggleVSSAO.ReadOnly = true;
            this.keytoggleVSSAO.Size = new System.Drawing.Size(100, 20);
            this.keytoggleVSSAO.TabIndex = 54;
            this.keytoggleVSSAO.TabStop = false;
            this.keytoggleVSSAO.Text = "NumPad2";
            this.toolTip.SetToolTip(this.keytoggleVSSAO, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleVSSAO.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleSMAA
            // 
            this.enabletoggleSMAA.AutoSize = true;
            this.enabletoggleSMAA.Location = new System.Drawing.Point(367, 141);
            this.enabletoggleSMAA.Name = "enabletoggleSMAA";
            this.enabletoggleSMAA.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleSMAA.TabIndex = 53;
            this.enabletoggleSMAA.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleSMAA, "Enable/disable the keybinding.");
            this.enabletoggleSMAA.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 141);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 13);
            this.label28.TabIndex = 52;
            this.label28.Text = "Toggle SMAA";
            // 
            // keytoggleSMAA
            // 
            this.keytoggleSMAA.Location = new System.Drawing.Point(388, 138);
            this.keytoggleSMAA.Name = "keytoggleSMAA";
            this.keytoggleSMAA.ReadOnly = true;
            this.keytoggleSMAA.Size = new System.Drawing.Size(100, 20);
            this.keytoggleSMAA.TabIndex = 51;
            this.keytoggleSMAA.TabStop = false;
            this.keytoggleSMAA.Text = "NumPad1";
            this.toolTip.SetToolTip(this.keytoggleSMAA, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleSMAA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleHUD
            // 
            this.enabletoggleHUD.AutoSize = true;
            this.enabletoggleHUD.Location = new System.Drawing.Point(367, 115);
            this.enabletoggleHUD.Name = "enabletoggleHUD";
            this.enabletoggleHUD.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleHUD.TabIndex = 50;
            this.enabletoggleHUD.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleHUD, "Enable/disable the keybinding.");
            this.enabletoggleHUD.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 115);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 49;
            this.label29.Text = "Toggle HUD";
            // 
            // keytoggleHUD
            // 
            this.keytoggleHUD.Location = new System.Drawing.Point(388, 112);
            this.keytoggleHUD.Name = "keytoggleHUD";
            this.keytoggleHUD.ReadOnly = true;
            this.keytoggleHUD.Size = new System.Drawing.Size(100, 20);
            this.keytoggleHUD.TabIndex = 48;
            this.keytoggleHUD.TabStop = false;
            this.keytoggleHUD.Text = "RCONTROL";
            this.toolTip.SetToolTip(this.keytoggleHUD, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleHUD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletakeHudlessScreenshot
            // 
            this.enabletakeHudlessScreenshot.AutoSize = true;
            this.enabletakeHudlessScreenshot.Location = new System.Drawing.Point(367, 89);
            this.enabletakeHudlessScreenshot.Name = "enabletakeHudlessScreenshot";
            this.enabletakeHudlessScreenshot.Size = new System.Drawing.Size(15, 14);
            this.enabletakeHudlessScreenshot.TabIndex = 47;
            this.enabletakeHudlessScreenshot.TabStop = false;
            this.toolTip.SetToolTip(this.enabletakeHudlessScreenshot, "Enable/disable the keybinding.");
            this.enabletakeHudlessScreenshot.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(5, 89);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(132, 13);
            this.label30.TabIndex = 46;
            this.label30.Text = "Take HUDless screenshot";
            // 
            // keytakeHudlessScreenshot
            // 
            this.keytakeHudlessScreenshot.Location = new System.Drawing.Point(388, 86);
            this.keytakeHudlessScreenshot.Name = "keytakeHudlessScreenshot";
            this.keytakeHudlessScreenshot.ReadOnly = true;
            this.keytakeHudlessScreenshot.Size = new System.Drawing.Size(100, 20);
            this.keytakeHudlessScreenshot.TabIndex = 45;
            this.keytakeHudlessScreenshot.TabStop = false;
            this.keytakeHudlessScreenshot.Text = "Next";
            this.toolTip.SetToolTip(this.keytakeHudlessScreenshot, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytakeHudlessScreenshot.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleBorderlessFullscreen
            // 
            this.enabletoggleBorderlessFullscreen.AutoSize = true;
            this.enabletoggleBorderlessFullscreen.Location = new System.Drawing.Point(367, 63);
            this.enabletoggleBorderlessFullscreen.Name = "enabletoggleBorderlessFullscreen";
            this.enabletoggleBorderlessFullscreen.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleBorderlessFullscreen.TabIndex = 44;
            this.enabletoggleBorderlessFullscreen.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleBorderlessFullscreen, "Enable/disable the keybinding.");
            this.enabletoggleBorderlessFullscreen.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 63);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "Toggle borderless fullscreen";
            // 
            // keytoggleBorderlessFullscreen
            // 
            this.keytoggleBorderlessFullscreen.Location = new System.Drawing.Point(388, 60);
            this.keytoggleBorderlessFullscreen.Name = "keytoggleBorderlessFullscreen";
            this.keytoggleBorderlessFullscreen.ReadOnly = true;
            this.keytoggleBorderlessFullscreen.Size = new System.Drawing.Size(100, 20);
            this.keytoggleBorderlessFullscreen.TabIndex = 42;
            this.keytoggleBorderlessFullscreen.TabStop = false;
            this.keytoggleBorderlessFullscreen.Text = "F8";
            this.toolTip.SetToolTip(this.keytoggleBorderlessFullscreen, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleBorderlessFullscreen.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleCursorVisibility
            // 
            this.enabletoggleCursorVisibility.AutoSize = true;
            this.enabletoggleCursorVisibility.Location = new System.Drawing.Point(367, 37);
            this.enabletoggleCursorVisibility.Name = "enabletoggleCursorVisibility";
            this.enabletoggleCursorVisibility.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleCursorVisibility.TabIndex = 41;
            this.enabletoggleCursorVisibility.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleCursorVisibility, "Enable/disable the keybinding.");
            this.enabletoggleCursorVisibility.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 37);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(110, 13);
            this.label26.TabIndex = 40;
            this.label26.Text = "Toggle cursor visibility";
            // 
            // keytoggleCursorVisibility
            // 
            this.keytoggleCursorVisibility.Location = new System.Drawing.Point(388, 34);
            this.keytoggleCursorVisibility.Name = "keytoggleCursorVisibility";
            this.keytoggleCursorVisibility.ReadOnly = true;
            this.keytoggleCursorVisibility.Size = new System.Drawing.Size(100, 20);
            this.keytoggleCursorVisibility.TabIndex = 39;
            this.keytoggleCursorVisibility.TabStop = false;
            this.keytoggleCursorVisibility.Text = "F7";
            this.toolTip.SetToolTip(this.keytoggleCursorVisibility, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleCursorVisibility.TextChanged += new System.EventHandler(this.trackBars_ValueChanged);
            this.keytoggleCursorVisibility.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // enabletoggleCursorCapture
            // 
            this.enabletoggleCursorCapture.AutoSize = true;
            this.enabletoggleCursorCapture.Location = new System.Drawing.Point(367, 11);
            this.enabletoggleCursorCapture.Name = "enabletoggleCursorCapture";
            this.enabletoggleCursorCapture.Size = new System.Drawing.Size(15, 14);
            this.enabletoggleCursorCapture.TabIndex = 38;
            this.enabletoggleCursorCapture.TabStop = false;
            this.toolTip.SetToolTip(this.enabletoggleCursorCapture, "Enable/disable the keybinding.");
            this.enabletoggleCursorCapture.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(111, 13);
            this.label25.TabIndex = 37;
            this.label25.Text = "Toggle cursor capture";
            // 
            // keytoggleCursorCapture
            // 
            this.keytoggleCursorCapture.Location = new System.Drawing.Point(388, 8);
            this.keytoggleCursorCapture.Name = "keytoggleCursorCapture";
            this.keytoggleCursorCapture.ReadOnly = true;
            this.keytoggleCursorCapture.Size = new System.Drawing.Size(100, 20);
            this.keytoggleCursorCapture.TabIndex = 36;
            this.keytoggleCursorCapture.TabStop = false;
            this.keytoggleCursorCapture.Text = "F6";
            this.toolTip.SetToolTip(this.keytoggleCursorCapture, "Select the textbox and then press the button you want to bind.\r\nSome buttons are " +
        "not supported, namely the standard keyboard keys in Dark Souls.");
            this.keytoggleCursorCapture.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyBindings_KeyUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gameLaunchButton);
            this.panel1.Controls.Add(this.applicationLoadButton);
            this.panel1.Controls.Add(this.applicationSaveButton);
            this.panel1.Controls.Add(this.dsFixLoadButton);
            this.panel1.Controls.Add(this.dsFixSaveButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 343);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(565, 34);
            this.panel1.TabIndex = 0;
            // 
            // gameLaunchButton
            // 
            this.gameLaunchButton.Location = new System.Drawing.Point(181, 3);
            this.gameLaunchButton.Name = "gameLaunchButton";
            this.gameLaunchButton.Size = new System.Drawing.Size(75, 23);
            this.gameLaunchButton.TabIndex = 4;
            this.gameLaunchButton.Text = "Launch DS";
            this.gameLaunchButton.UseVisualStyleBackColor = true;
            this.gameLaunchButton.Click += new System.EventHandler(this.button6_Click);
            // 
            // applicationLoadButton
            // 
            this.applicationLoadButton.Location = new System.Drawing.Point(420, 3);
            this.applicationLoadButton.Name = "applicationLoadButton";
            this.applicationLoadButton.Size = new System.Drawing.Size(142, 23);
            this.applicationLoadButton.TabIndex = 3;
            this.applicationLoadButton.TabStop = false;
            this.applicationLoadButton.Text = "Load application settings";
            this.toolTip.SetToolTip(this.applicationLoadButton, "Loads settings previously saved.");
            this.applicationLoadButton.UseVisualStyleBackColor = true;
            this.applicationLoadButton.Click += new System.EventHandler(this.appLoadButton_Click);
            // 
            // applicationSaveButton
            // 
            this.applicationSaveButton.Location = new System.Drawing.Point(272, 3);
            this.applicationSaveButton.Name = "applicationSaveButton";
            this.applicationSaveButton.Size = new System.Drawing.Size(142, 23);
            this.applicationSaveButton.TabIndex = 2;
            this.applicationSaveButton.TabStop = false;
            this.applicationSaveButton.Text = "Save application settings";
            this.toolTip.SetToolTip(this.applicationSaveButton, "Save current setup to application-specific settings.\r\nLets you load your settings" +
        " even after a new DsFix is released.");
            this.applicationSaveButton.UseVisualStyleBackColor = true;
            this.applicationSaveButton.Click += new System.EventHandler(this.appSaveButton_Click);
            // 
            // dsFixLoadButton
            // 
            this.dsFixLoadButton.Location = new System.Drawing.Point(86, 3);
            this.dsFixLoadButton.Name = "dsFixLoadButton";
            this.dsFixLoadButton.Size = new System.Drawing.Size(80, 23);
            this.dsFixLoadButton.TabIndex = 1;
            this.dsFixLoadButton.TabStop = false;
            this.dsFixLoadButton.Text = "Load/Reload";
            this.toolTip.SetToolTip(this.dsFixLoadButton, "Load settings from the DsFix.ini file.");
            this.dsFixLoadButton.UseVisualStyleBackColor = true;
            this.dsFixLoadButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // dsFixSaveButton
            // 
            this.dsFixSaveButton.Location = new System.Drawing.Point(5, 3);
            this.dsFixSaveButton.Name = "dsFixSaveButton";
            this.dsFixSaveButton.Size = new System.Drawing.Size(75, 23);
            this.dsFixSaveButton.TabIndex = 0;
            this.dsFixSaveButton.TabStop = false;
            this.dsFixSaveButton.Text = "Save";
            this.toolTip.SetToolTip(this.dsFixSaveButton, "Save settings to the DsFix.ini file.");
            this.dsFixSaveButton.UseVisualStyleBackColor = true;
            this.dsFixSaveButton.Click += new System.EventHandler(this.dsFixSaveButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "DSfix|dsfix.ini";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 377);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(581, 415);
            this.MinimumSize = new System.Drawing.Size(581, 415);
            this.Name = "Form1";
            this.Text = "DSfix Portable ~Morten242 visual tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.graphicOptions.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dofBlurAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dofOverrideResNum)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.presentHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.presentWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FPSthreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxFPS)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.hudOptions.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomRightCorner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topLeftCorner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomLeftCorner)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hudScaleFactor)).EndInit();
            this.windowMouseOptions.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.saveBackupOptions.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxBackups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backupInterval)).EndInit();
            this.textureOverrideOptions.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.miscOptions.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logLevel)).EndInit();
            this.keyBindTab.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.keyPanel.ResumeLayout(false);
            this.keyPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage hudOptions;
        private System.Windows.Forms.TabPage windowMouseOptions;
        private System.Windows.Forms.TabPage saveBackupOptions;
        private System.Windows.Forms.TabPage textureOverrideOptions;
        private System.Windows.Forms.TabPage miscOptions;
        private System.Windows.Forms.TabPage graphicOptions;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button dsFixLoadButton;
        private System.Windows.Forms.Button dsFixSaveButton;
        private System.Windows.Forms.Button applicationSaveButton;
        private System.Windows.Forms.Button applicationLoadButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox enableHudMod;
        private System.Windows.Forms.CheckBox minimalHud;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TrackBar bottomRightCorner;
        private System.Windows.Forms.TrackBar bottomLeftCorner;
        private System.Windows.Forms.TrackBar topLeftCorner;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox captureCursor;
        private System.Windows.Forms.CheckBox disableCursor;
        private System.Windows.Forms.CheckBox borderlessFullscreen;
        private System.Windows.Forms.CheckBox enableBackups;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown maxBackups;
        private System.Windows.Forms.NumericUpDown backupInterval;
        private System.Windows.Forms.ComboBox timeUnit;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox enableTextureOverride;
        private System.Windows.Forms.CheckBox enableTextureDumping;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox skipIntro;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox screenshotDir;
        private System.Windows.Forms.ComboBox overrideLanguage;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox dinput8dllWrapper;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown logLevel;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage keyBindTab;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Panel keyPanel;
        private System.Windows.Forms.CheckBox enabletoggle30FPSLimit;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox keytoggle30FPSLimit;
        private System.Windows.Forms.CheckBox enablereloadSSAOEffect;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox keyreloadSSAOEffect;
        private System.Windows.Forms.CheckBox enabletoggleHudChange;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox keytoggleHudChange;
        private System.Windows.Forms.CheckBox enabletoggleDofGauss;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox keytoggleDofGauss;
        private System.Windows.Forms.CheckBox enabletoggleVSSAO;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox keytoggleVSSAO;
        private System.Windows.Forms.CheckBox enabletoggleSMAA;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox keytoggleSMAA;
        private System.Windows.Forms.CheckBox enabletoggleHUD;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox keytoggleHUD;
        private System.Windows.Forms.CheckBox enabletakeHudlessScreenshot;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox keytakeHudlessScreenshot;
        private System.Windows.Forms.CheckBox enabletoggleBorderlessFullscreen;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox keytoggleBorderlessFullscreen;
        private System.Windows.Forms.CheckBox enabletoggleCursorVisibility;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox keytoggleCursorVisibility;
        private System.Windows.Forms.CheckBox enabletoggleCursorCapture;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox keytoggleCursorCapture;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown dofBlurAmount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown dofOverrideResNum;
        private System.Windows.Forms.CheckBox customDofOverrideRes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox enableDofScaling;
        private System.Windows.Forms.ComboBox dofOverrideRes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown maxFPS;
        private System.Windows.Forms.CheckBox unlockFPS;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox resolutionDrop;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button gameLaunchButton;
        private System.Windows.Forms.NumericUpDown FPSthreshold;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox aaType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ssaoStrength;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox aaQuality;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox ssaoScale;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox ssaoType;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.NumericUpDown presentHeight;
        private System.Windows.Forms.NumericUpDown presentWidth;
        private System.Windows.Forms.Button dsmFixGuiButton;
        private System.Windows.Forms.Label dsmFixGuiLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar hudScaleFactor;
    }
}

